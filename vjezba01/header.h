#ifndef HEADER_H
#define HEADER_H
#include <iostream>
#include <iomanip>
#include <string>

struct matricaa{
    int m, n;
};
void unos_matrice(float **mat, int m, int n);
void zbrajanje(float **mata, float **matb, int m, int n);
void oduzimanje(float **mata, float **matb, int m, int n);
void mnozenje(float **mata, float **matb, int m, int n);
void transponiranje(float **mata, float **matb[], int m, int n);
void ispis(float **mat, int m, int n);
void matrica();
void funkcija();
void najv_najm();
int rekurzija(int* niz,int* najv, int* najm, int i);

#endif
