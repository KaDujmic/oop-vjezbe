#include "header.h"
#include <iostream>
#include <iomanip>
using namespace std;

void unos_matrice(float **mat, int m, int n)
{
    int i = 0 , j = 0;
    while (m < i) 
    {
        while (n < j) 
        {
            cout << "Unesite element matrice" << endl;
            cin >> mat[i][j];
            j++;
        }
        j = 0;
        i++;
    }
}

void zbrajanje(float **mata, float **matb, int m, int n)
{
    for(int i = 0; i < m; ++i)
        for(int j = 0; j < n; ++j)
            mata[i][j] = mata[i][j] + matb[i][j];
}

void oduzimanje(float **mata, float **matb, int m, int n)
{
    for(int i = 0; i < m; ++i)
        for(int j = 0; j < n; ++j)
            mata[i][j] = mata[i][j] - matb[i][j];
}

void mnozenje(float **mata, float **matb, int m, int n)
{
    for(int i = 0; i < m; ++i)
        for(int j = 0; j < n; ++j)
            mata[i][j] = mata[i][j] * matb[i][j];
}

void transponiranje(float **mata, float **matb, int m, int n)
{
    for (int i = 0; i < m; ++i)
      for (int j = 0; j < n; ++j) 
      {
        mata[j][i] = matb[i][j];
      }
}

void ispis(float **mat, int m, int n)
{
    for(int i = 0; i < m; ++i)
    {
        for(int j = 0; j < n; ++j)
        {
            cout << mat[i][j] << " ";
            if (j == n - 1)
                cout << endl;
        }
    }    
}

void matrica()
{
    char operacija;
    int m , n, m1, n1;
    char odabir;
    cout << "Unesite dimenzije prve matrice: " << endl;
    cin >> m >> n;
    cout << "Unesite dimenzije druge matrice: " << endl;
    cin >> m1 >> n1;
    // ALOKACIJA ZA PRVU MATRICU
    float** matrica_a;
    matrica_a = new float*[m];
    for(int i = 0; i < m; i++)
        matrica_a[i] = new float[n];;
    // ALOKACIJA ZA DRUGU MATRICU
    float** matrica_b;
    matrica_b = new float*[m1];
    for(int i = 0; i < m1; i++)
        matrica_b[i] = new float[n1];
    unos_matrice(matrica_a,m,n);
    unos_matrice(matrica_b,m1,n1);
    cout << "Unesite operaciju(t,m,z,o): " << operacija << endl;
    cin >> operacija;
    if (operacija == 'z')
    {
        if (m==m1 && n==n1)
        {
            zbrajanje(matrica_a,matrica_b,m,n);
            ispis(matrica_a,m,n);
        }
        else
            cout << "Matrice nisu istog tipa ne mogu se zbrajati!!" << endl;
    }
    else if (operacija == 'm')
    {
        if (m==n)
        {
            mnozenje(matrica_a,matrica_b,m,n);
            ispis(matrica_a,m,n);
        }
        else if (m==n1)
        {
            mnozenje(matrica_a,matrica_b,m,n1);
            ispis(matrica_a,m,n1);
        }
        else 
            cout << "Matrice nisu ulancane ili kvadratne, ne mogu se mnoziti!!" << endl;
    }
    else if (operacija == 'o')
    {
        if (m==m1 && n==n1)
        {
            oduzimanje(matrica_a,matrica_b,m,n);
            ispis(matrica_a,m,n);
        }
        else
            cout << "Matrice nisu istog tipa ne mogu se oduzimati" << endl;
    }
    else if (operacija == 't')
    {
        cout << "Koju matricu zelite transponirati A ili B?" << endl;
        cin >> odabir;
        float** transponirana;
        transponirana = new float*[m];
        for(int i = 0; i < m; i++)
            transponirana[i] = new float[n];;
        if (odabir == 'A')
        {
            transponiranje(transponirana,matrica_a,m,n);
            ispis(transponirana,n,m);
        }
        else if (odabir == 'B')
        {
            transponiranje(transponirana,matrica_b,m,n);
            ispis(transponirana,n,m);
        }
        else
            cout << "Pogrešan unos." << endl;
    }
    for(int i = 0; i < m; i++)
        delete [] matrica_a[i];
    delete [] matrica_a;
    for(int i = 0; i < m; i++)
        delete [] matrica_b[i];
    delete [] matrica_b;
}